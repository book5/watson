# 暗空公園
位於合歡山的暗空公園為亞洲第三個國際暗空協會(IDA)認證的國際暗空公園，
是一個不太有光害、很適合觀星的景點。

* 目錄
    * [位置](#位置)
    * [住宿與交通](#住宿與交通)
    * [觀星日期](#觀星日期)
    * [周邊行程](#周邊行程)

## 位置
南投縣合歡山，台 14 甲線 22.1 - 37.1 公里，包括鳶峰、昆陽、武嶺、松雪樓等觀星的地點
([Google Map](https://www.google.com.tw/maps/dir/%E5%B0%8F%E9%A2%A8%E5%8F%A3+972%E8%8A%B1%E8%93%AE%E7%B8%A3%E7%A7%80%E6%9E%97%E9%84%89/546%E5%8D%97%E6%8A%95%E7%B8%A3%E4%BB%81%E6%84%9B%E9%84%89%E9%B3%B6%E5%B3%B0%E8%A7%80%E6%99%AF%E5%8F%B0/@24.133572,121.2645568,15z/data=!4m14!4m13!1m5!1m1!1s0x346892e61af04f4f:0x1a7a34e26996cd82!2m2!1d121.29039!2d24.1646302!1m5!1m1!1s0x3468eb673ca8a315:0xfd39c8fa6b4d2f58!2m2!1d121.2373438!2d24.1176961!3e0?hl=zh-TW))
。

## 交通與住宿
* 交通:
    * 自行前往 ([Google Map](https://www.google.com.tw/maps/dir/%E8%87%BA%E4%B8%AD%E8%BB%8A%E7%AB%99+400%E5%8F%B0%E4%B8%AD%E5%B8%82%E4%B8%AD%E5%8D%80%E5%8F%B0%E7%81%A3%E5%A4%A7%E9%81%93%E4%B8%80%E6%AE%B51%E8%99%9F/546%E5%8D%97%E6%8A%95%E7%B8%A3%E4%BB%81%E6%84%9B%E9%84%89%E9%B3%B6%E5%B3%B0%E8%A7%80%E6%99%AF%E5%8F%B0/@23.9945464,121.028653,16.99z/am=t/data=!4m14!4m13!1m5!1m1!1s0x34693d1438fb3d3f:0xb7b4ebd02f1906b6!2m2!1d120.6866659!2d24.1372593!1m5!1m1!1s0x3468eb673ca8a315:0xfd39c8fa6b4d2f58!2m2!1d121.2373438!2d24.1176961!3e0?hl=zh-TW)): 
        * 台中火車站
        * 進入大里區
        * 走台 74 縣
        * 併入國 3
        * 霧峰系統出口(國 6 出口)下交流道
        * 走國 6
        * 國 6 埔里端進入台 14 甲線
        
    * 市區 => 清境 => 鳶峰
        * 台中火車站(干城)/台中高鐵 => 清境:
            * [南投客運](http://www.ntbus.com.tw/cjfm.html) (約 2.5 hrs)
            * [KKDay 共乘](https://www.kkday.com/zh-tw/product/1756?cid=2003&ud1=32436&ud2=1756) (NT$ 500 / 單程)
            * [Klook 共乘](https://www.klook.com/zh-TW/activity/19127-shared-transfer-service-cingjing-taichung/?gclid=CjwKCAiA-f78BRBbEiwATKRRBE2immYKQApSvdBSwOmwEClTrEyXQVpCQ6uvkBRYVCfdaD295pbNChoCOBMQAvD_BwE&gclsrc=aw.ds) (NT$ 460 / 單程)
        * 清境 => 鳶峰
            * [Klook 共乘](https://www.klook.com/zh-TW/activity/46416-share-transfer-tour-chingjing-to-hehuan-mountain-dark-sky-park/) (NT$ 700 / 來回)

* 清境住宿:
    > 列舉最低房價，會依入住日期有所差異
    * [清境楓葉山莊](https://www.agoda.com/zh-tw/maple-leaf-holiday-villa/hotel/nantou-tw.html?finalPriceView=0&isShowMobileAppPrice=false&cid=1736111&tag=1000239_1004766900883&numberOfBedrooms=&familyMode=false&isAgMse=false&ccallout=false&defdate=false&voucherMode=false&adults=1&children=0&rooms=1&maxRooms=9&checkIn=2020-11-3&childAges=&defaultChildAge=8&travellerType=0&los=1&searchrequestid=a78c1277-fa59-4039-aed8-ff9ce2ab3f55)
        * 評分: 7.3 / 10 (966 筆)
        * 標準雙人房: NT$ 1346 / 人日
    * [娜嚕彎渡假景觀木屋民宿](https://www.agoda.com/zh-tw/nalu-wan-villa/hotel/nantou-tw.html?finalPriceView=0&isShowMobileAppPrice=false&cid=1736111&tag=1000239_1004766900893&numberOfBedrooms=&familyMode=false&isAgMse=false&ccallout=false&defdate=false&voucherMode=false&adults=2&children=0&rooms=1&maxRooms=9&childAges=&defaultChildAge=8&travellerType=1&los=1&searchrequestid=a78132c7-9f61-45ba-b6b3-f9902ddd405c&checkin=2020-12-22)
        * 評分: 7.3 / 10 (536 筆)
        * 標準雙人房: NT$ 2100
    * [清境佛羅倫斯渡假山莊](https://www.agoda.com/zh-tw/florence-resort-villa/hotel/nantou-tw.html?asq=P%2FqHi77RAEmTXQB%2BCic%2B6VYxj2NQ4JOukw%2FqFPKhCQHyZvCeA7NW0TEpyNp1386upHOx%2FBfBGiBy%2FZvpBcUaXts9bykPJ0UYkelR4YG%2FtAtinn4nLWsPMq8Wq3zmunLefIDaGQVwVr0p8VGAgUIExNX%2BLTLQpFDnBugEEhZpLmKvaiYMPOUeRUASlRew4pdJSBBv%2FrHiHqBQk6vHl6MZIuGhUAc%2FDMrwrhrnK9emNh4%3D&hotel=624311&cid=1736111)
        * 評分: 8.0 / 10 (203 筆)
        * 嗯...
    * [清境天星渡假山莊](https://www.booking.com/hotel/tw/cingjing-star-villa.zh-tw.html?aid=1646003;sid=98e505c355ee9f384096f94e5f5871ca;all_sr_blocks=68418202_202230425_0_1_0;checkin=2020-11-20;checkout=2020-11-21;dest_id=900051920;dest_type=city;dist=0;group_adults=2;group_children=0;hapos=1;highlighted_blocks=68418202_202230425_0_1_0;hp_group_set=0;hpos=1;no_rooms=1;room1=A%2CA;sb_price_type=total;sr_order=popularity;sr_pri_blocks=68418202_202230425_0_1_0__434789;srepoch=1604335489;srpvid=cc9275c092fb00ca;type=total;ucfs=1&)
        * 評分: 8.5 / 10 (600 筆)
        * 標準雙人房: NT$ 4348
    * [來福居民宿](https://asiayo.com/zh-tw/view/tw/nantou-county/24491/?aff_id=134)
        * 評分: 4.5 / 5 (4 筆)
        * 景觀雅緻二人房: NT$ 4348
    * [楓丹白露](https://www.agoda.com/zh-tw/fendan-b-b/hotel/nantou-tw.html?finalPriceView=0&isShowMobileAppPrice=false&cid=1736111&tag=1000239_1004766900953&numberOfBedrooms=&familyMode=false&isAgMse=false&ccallout=false&defdate=false&voucherMode=false&adults=2&children=0&rooms=1&maxRooms=9&childAges=&defaultChildAge=8&travellerType=-1&los=1&searchrequestid=052c04a3-4908-4716-a384-523907fc4975&checkin=2020-12-22)
        * 評分: 8.7 / 10 (128 筆)
        * 家庭房(大床): NT$ 4974
    * [清境西雅圖璀璨雙城](https://www.booking.com/hotel/tw/brilliant-twinsofseattle.zh-tw.html?aid=1646003&sid=98e505c355ee9f384096f94e5f5871ca&sb=1&src=hotel&src_elem=sb&error_url=https%3A%2F%2Fwww.booking.com%2Fhotel%2Ftw%2Fbrilliant-twinsofseattle.zh-tw.html%3Faid%3D1646003%3Bsid%3D98e505c355ee9f384096f94e5f5871ca%3Bdist%3D0%3Broom1%3DA%252CA%3Bsb_price_type%3Dtotal%3Btype%3Dtotal%26%3B&highlighted_hotels=639817&ssne=%E4%BB%81%E6%84%9B%E9%84%89&ssne_untouched=%E4%BB%81%E6%84%9B%E9%84%89&city=900051920&checkin_year=2020&checkin_month=11&checkin_monthday=7&checkout_year=2020&checkout_month=11&checkout_monthday=8&no_rooms=1&group_adults=2&group_children=0&from_sf=1#_)
        * 評分: 8.0 / 10 (463 筆)
        * 標準雙床間- A棟: NT$ 4500
    * [清境維也納庭園民宿](https://www.booking.com/hotel/tw/cingjing-vienna-pleasance-cottage.zh-tw.html?aid=318615;label=Chinese_Traditional_ZH-XT_29561951425-89UEDgKFJnufpFpstF1fHQS113415627145%3Apl%3Ata%3Ap1%3Ap2%3Aac%3Aap%3Aneg%3Afi4484722465%3Atidsa-331215073644%3Alp1012825%3Ali%3Adec%3Adm;sid=98e505c355ee9f384096f94e5f5871ca;all_sr_blocks=70867711_100021421_0_1_0;checkin=2020-11-06;checkout=2020-11-07;dest_id=900051920;dest_type=city;dist=0;group_adults=2;group_children=0;hapos=1;highlighted_blocks=70867711_100021421_0_1_0;hpos=1;no_rooms=1;room1=A%2CA;sb_price_type=total;sr_order=popularity;sr_pri_blocks=70867711_100021421_0_1_0__301500;srepoch=1604336402;srpvid=467a77880e6700c9;type=total;ucfs=1&#RD70867711)
        * 評分: 8.6 / 10 (481 筆)
        * 加大雙人床房: NT$ 3015

## 觀星日期
* 銀河: 06 月 - 08 月
* 流星雨:
    * [英仙座流星雨](https://www.tam.museum/astronomy/forecast_detail.php?lang=tw&id=452): 07/17 - 08/24
    * [獵戶座流星雨](https://www.tam.museum/astronomy/forecast_detail.php?lang=tw&id=469): 10/20 - 10/21
    * [雙子座流星雨](https://www.tam.museum/astronomy/forecast_detail.php?lang=tw&id=391): 12/04 - 12/17

## 周邊行程
> Note, 「★」 與暗黑公園較有距離

* 吃吃喝喝
    * [在地人攻略 - PTT](https://www.ptt.cc/bbs/Gossiping/M.1584353954.A.CC0.html)
        * [埔里鹹油條](https://www.google.com.tw/maps/place/%E5%9F%94%E9%87%8C%E9%B9%B9%E6%B2%B9%E6%A2%9D/@23.9596233,120.9659442,16z/data=!4m8!1m2!2m1!1z5Z-U6YeM6bm55rK55qKd!3m4!1s0x3468d990ab02593b:0x9d02eab412233c0e!8m2!3d23.9596233!4d120.9703216?hl=zh-TW)
        * [大麵銘](https://www.google.com.tw/maps/place/%E5%A4%A7%E9%BA%B5%E9%8A%98/@23.968089,120.9686473,17z/data=!3m1!4b1!4m5!3m4!1s0x3468d99dd1842307:0xe4f60ff742a28914!8m2!3d23.968089!4d120.970836?hl=zh-TW)
        * [菊肉圓](https://www.google.com.tw/maps/place/%E8%8F%8A%E8%82%89%E5%9C%93/@23.9656961,120.9618991,16.74z/data=!4m5!3m4!1s0x3468d9a288c4ee43:0xa02366827c5fec4b!8m2!3d23.963825!4d120.963914?hl=zh-TW)
        * [小上海阿和小籠包](https://www.google.com.tw/maps/place/%E5%B0%8F%E4%B8%8A%E6%B5%B7%E9%98%BF%E5%92%8C%E5%B0%8F%E7%B1%A0%E5%8C%85/@23.9666042,120.9647566,17z/data=!3m1!4b1!4m5!3m4!1s0x3468d99f334ed34d:0xaa61466de5a49da5!8m2!3d23.9666042!4d120.9669453?hl=zh-TW)
    * [肆盒院](https://www.google.com.tw/maps/place/%E8%82%86%E7%9B%92%E9%99%A2/@23.9607331,120.9576569,17z/data=!3m1!4b1!4m5!3m4!1s0x3468d9bc952157f9:0x82a7f42fbfbf074a!8m2!3d23.9607331!4d120.9598456?hl=zh-TW): [食評](https://sunmoonlake.welcometw.com/%E5%9F%94%E9%87%8C%E7%BE%8E%E9%A3%9F-%EF%BD%9C%E8%B6%85%E5%A4%AF%E3%80%8A%E5%9F%94%E9%87%8C%E5%9C%A8%E5%9C%B0%E7%BE%8E%E9%A3%9F%E3%80%8B%E5%A4%A7%E9%9B%86%E5%90%88%EF%BC%8C%E7%94%A8%E4%BD%A0%E7%9A%84/)
    * [桃木河岸小棧](https://www.google.com.tw/maps/place/%E6%A1%83%E7%B1%B3%E6%B2%B3%E5%B2%B8%E5%B0%8F%E6%A3%A7/@23.945954,120.9350103,17z/data=!3m1!4b1!4m5!3m4!1s0x3468d83244be85a5:0x8d41f32eab4f38ff!8m2!3d23.945954!4d120.937199?hl=zh-TW): [官網](https://0492914968.okgo.tw/)
    * [水母吃乳酪](https://www.google.com/maps?ll=23.980739,121.012294&z=15&t=m&hl=zh-TW&gl=TW&mapclient=embed&cid=7984947415231487545): [官網](http://www.jellyfish.com.tw/store.php)
    <br />
    <img src="https://i.imgur.com/qbH3v8Il.jpg" height="300" alt="埔里鹹油條"/>
    <img src="https://i2.wp.com/aniseblog.tw/wp-content/uploads/2013/02/1491195114-a4f04e2c58d88cde16a7b1fe5fbdea54.jpg?zoom=1.25&w=620&ssl=1" height="300" alt="大麵銘"/>
    <img src="https://pic.pimg.tw/cndjourney/1599666989-1565062369-g_n.jpg" height="300" alt="菊肉圓"/>
    <img src="https://www.bigfang.tw/wp-content/uploads/2017/09/1504407356-3880dc8a52f6410add7de920940159a3.jpg" height="300" alt="小上海阿和小籠包"/>
    <img src="https://i0.wp.com/sunmoonlake.welcometw.com/wp-content/uploads/2020/05/w1.jpg" height="300" alt="肆盒院"/>
    <img src="https://farm2.staticflickr.com/1633/25180390883_6c5f287eee_z.jpg" height="300" alt="桃木河岸小棧"/>
    <img src="https://cdn.walkerland.com.tw/images/upload/poi/p95815/m73907/428ee70413509bc9bc995aa27bb7aefabb20f4f0.jpg" height="300" alt="水母吃乳酪"/>
    <br />
    * 同場加映: [豐原](https://www.google.com.tw/maps/@24.2512304,120.7205711,15.71z/data=!4m2!6m1!1s1244oED76E-MLWgtlYw5wS0ftsqqiMUVV?hl=zh-TW) (硬要 XD)
        > 千萬不要吃廟東裡的「清水排骨酥麵」
* 景點
    * [築樂](https://www.google.com.tw/maps/place/%E7%AF%89%E6%A8%82/@23.9553438,120.9266506,14.67z/data=!4m5!3m4!1s0x3468d83316d93157:0xace8a2dd32eeb895!8m2!3d23.9489368!4d120.9400095?hl=zh-TW)
        * 漂亮舒適的日式庭院簡餐咖啡聽 (好不吸引人的描述 XD) <br />
        <img src="https://pic.pimg.tw/cindy6732/1570024350-3402159358.jpg" height="300" />
        <img src="https://i1.wp.com/yoke918.com/wp-content/uploads/2019/09/19096521.jpg?resize=618%2C491&quality=100&ssl=1" height="300" /> <br />
        * 介紹
            * [【南投埔里景點】築樂｜猶如置身日本的山林秘境，小巧唯美的落羽松林，咖哩飯、咖啡和甜點，還有露營區，桃米生態村](http://cindypark.cc/blog/post/32980927-%E3%80%90%E5%8D%97%E6%8A%95%E5%9F%94%E9%87%8C%E6%99%AF%E9%BB%9E%E3%80%91%E7%AF%89%E6%A8%82%EF%BD%9C%E7%8C%B6%E5%A6%82%E7%BD%AE%E8%BA%AB%E6%97%A5%E6%9C%AC%E7%9A%84%E5%B1%B1%E6%9E%97)
            * [南投埔里景點》築樂 日式景觀餐廳．一起來去感受濃濃日本味](https://yoke918.com/zhule/)
            * [【埔里】築樂 南投日式庭院景觀餐廳 成群落羽松好夢幻 還有大片草地可以溜小孩](https://lyes.tw/blog/post/32995303-%E7%AF%89%E6%A8%82)
    * [紙教堂](https://www.google.com.tw/maps/place/%E7%B4%99%E6%95%99%E5%A0%82+Paper+Dome/@23.94142,120.9248669,17z/data=!3m1!4b1!4m5!3m4!1s0x3468d5f961d27049:0x2b85afe238738be!8m2!3d23.9413807!4d120.9270419?hl=zh-TW)
        * 從大阪來的紙做教堂，夜幕中經過燈光的點綴與湖面倒影顯得格外漂亮 (夜幕低垂之後比教推薦) <br />
        <img src="http://www.pse100i.idv.tw/m/1zngyty/1zngyty001.jpg" height="300" />
        <img src="https://farm4.static.flickr.com/3540/3447619886_bb84964ea0_o.jpg" height="300" /> <br />
        * 介紹
            * [埔里紙教堂](http://www.pse100i.idv.tw/m/1zngyty/1zngyty001.html)
            * [南投景點》埔里桃米生態村紙教堂，漫步優雅綠色池畔園區~](https://bunnyann.com/bunnyann128/)
            * [【南投埔里景點、餐廳】紙教堂_賞荷、吃荷葉飯](https://as660707.pixnet.net/blog/post/373190243)
    * [★ 雙龍瀑布彩虹吊橋](https://www.google.com.tw/maps/place/%E9%9B%99%E9%BE%8D%E4%B8%83%E5%BD%A9%E5%90%8A%E6%A9%8B%E3%80%90%E9%9C%80%E9%A0%90%E7%B4%84%E3%80%91/@23.7807642,120.9481584,17z/data=!3m1!4b1!4m5!3m4!1s0x3468d4e0ac7333d3:0x31e051e206cd4184!8m2!3d23.7807593!4d120.9503471?hl=zh-TW)
        * 很漂亮也很舒壓的彩虹吊橋，如果有時間覺得很適合去走走 (看了介紹都心動 XD) <br />
        <img src="https://i0.wp.com/sunmoonlake.welcometw.com/wp-content/uploads/2020/01/iamzoechang-1.jpg" height="300" />
        <img src="https://pic.pimg.tw/car0126/1577554340-2818648260.jpg" height="300" /> <br />
        * 介紹
            * [雙龍瀑布七彩吊橋 | 購票、交通、周邊景點推薦【最強旅遊攻略】](https://sunmoonlake.welcometw.com/%E9%9B%99%E9%BE%8D%E5%90%8A%E6%A9%8B-faq/)
            * [【需預約】雙龍瀑布七彩吊橋入場券-個人票(全票100元/優待票70元/特惠票30元)](https://nantou.welcometw.com/tour/N9Q6)
            * [雙龍瀑布七彩吊橋(雙龍瀑布水源吊橋)│南投景點》南投新景點 全台最長最深的雙龍七彩吊橋 ...](https://car0126.pixnet.net/blog/post/44710605)
            * 同場加映: [谷關-龍谷瀑布](https://cbg777.pixnet.net/blog/post/68361024-%5B%E6%BA%AF%E6%BA%AA%5D-%E9%9A%B1%E8%97%8F%E5%9C%A8%E5%BB%A2%E5%A2%9F%E5%BE%8C%E7%9A%84%E7%84%A1%E6%95%B5%E5%A3%AF%E8%A7%80%E7%9A%84%E9%BE%8D%E8%B0%B7%E5%A4%A7%E7%80%91%E5%B8%83)
